\contentsline {chapter}{Abstract}{v}{chapter*.1}%
\contentsline {chapter}{Contents}{vii}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Contributions}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Outline}{3}{section.1.2}%
\contentsline {chapter}{\numberline {2}Background}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Early Attribute Grammars}{6}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Statically Scheduled Attribute Grammar Categories}{7}{subsection.2.1.1}%
\contentsline {subsubsection}{Purely Synthesised Attribute Grammars}{7}{section*.3}%
\contentsline {subsubsection}{Non-Circular Attribute Grammars}{7}{section*.4}%
\contentsline {subsubsection}{Strongly (or Absolutely) Non-Circular Attribute Grammars}{7}{section*.5}%
\contentsline {subsubsection}{Ordered Attribute Grammars}{8}{section*.6}%
\contentsline {subsubsection}{Other Categories}{8}{section*.7}%
\contentsline {section}{\numberline {2.2}Dynamically Scheduled Attribute Grammars}{8}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Attribute Grammar Notations}{9}{subsection.2.2.1}%
\contentsline {subsubsection}{Silver}{10}{section*.8}%
\contentsline {subsubsection}{JastAdd}{10}{section*.9}%
\contentsline {subsubsection}{Kiama}{12}{section*.10}%
\contentsline {section}{\numberline {2.3}Attribute Grammar Extensions}{14}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Reference Attribute Grammars}{14}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Parameterised Attribute Grammars}{16}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Attribute Caching}{18}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Higher Order Attribute Grammars}{19}{subsection.2.3.4}%
\contentsline {subsubsection}{Higher Order Nodes in the Existing Tree}{21}{section*.11}%
\contentsline {subsection}{\numberline {2.3.5}Attribute Forwarding}{22}{subsection.2.3.5}%
\contentsline {subsection}{\numberline {2.3.6}Circular Attribute Grammars}{23}{subsection.2.3.6}%
\contentsline {subsection}{\numberline {2.3.7}Collection attributes}{24}{subsection.2.3.7}%
\contentsline {section}{\numberline {2.4}Previous Attempts}{26}{section.2.4}%
\contentsline {chapter}{\numberline {3}Saiga}{29}{chapter.3}%
\contentsline {section}{\numberline {3.1}Calculus}{29}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}An Underlying System}{30}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}All Types}{31}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Expression Language}{31}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Type Rules}{31}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Semantic Rules}{32}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Intrinsic, Structural, and Extrinsic Attributes}{33}{subsection.3.1.6}%
\contentsline {subsection}{\numberline {3.1.7}Simple Example}{34}{subsection.3.1.7}%
\contentsline {section}{\numberline {3.2}Discussion}{36}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Equation Selection and the Context Function}{36}{subsection.3.2.1}%
\contentsline {subsubsection}{Demonstration}{38}{section*.12}%
\contentsline {subsection}{\numberline {3.2.2}Equation Selection and Node Types}{39}{subsection.3.2.2}%
\contentsline {subsubsection}{Symbol-Based Equation Selection}{40}{section*.13}%
\contentsline {subsubsection}{Production-Based Equation Selection}{41}{section*.14}%
\contentsline {subsubsection}{Selector Notations}{42}{section*.15}%
\contentsline {subsubsection}{Selector Example}{45}{section*.16}%
\contentsline {subsubsection}{Partial Context Functions and Non-Terminating Evaluation}{45}{section*.17}%
\contentsline {subsection}{\numberline {3.2.3}Conditional Expressions}{46}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Multiple-Parameter Functions}{46}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}Tuples}{47}{subsection.3.2.5}%
\contentsline {subsection}{\numberline {3.2.6}Functions Returning Expressions}{48}{subsection.3.2.6}%
\contentsline {subsection}{\numberline {3.2.7}Option Types and the Null Node}{49}{subsection.3.2.7}%
\contentsline {subsection}{\numberline {3.2.8}The Multistep Relation}{50}{subsection.3.2.8}%
\contentsline {subsection}{\numberline {3.2.9}The Big Step Relation}{50}{subsection.3.2.9}%
\contentsline {section}{\numberline {3.3}Name Analysis Example}{51}{section.3.3}%
\contentsline {section}{\numberline {3.4}Conclusion}{53}{section.3.4}%
\contentsline {chapter}{\numberline {4}Saiga Extended}{55}{chapter.4}%
\contentsline {section}{\numberline {4.1}Parameterised Attributes}{55}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Expression Language}{56}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}All Types}{56}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Type Rules}{56}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Semantic Rules}{57}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}Name analysis Example Revisited}{57}{subsection.4.1.5}%
\contentsline {section}{\numberline {4.2}Attribute Caching}{59}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Expression Language}{60}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Type Rules}{60}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Semantic Rules}{60}{subsection.4.2.3}%
\contentsline {subsubsection}{A Changing Context Function}{61}{section*.18}%
\contentsline {subsubsection}{Caching Rules}{62}{section*.19}%
\contentsline {subsubsection}{Two Attribution Rules}{62}{section*.20}%
\contentsline {subsection}{\numberline {4.2.4}Caching Example}{62}{subsection.4.2.4}%
\contentsline {section}{\numberline {4.3}Discussion}{65}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Non-Parameterised Attributes}{65}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Attributes Returning Functions}{66}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Re-Caching Values and Simplicity}{67}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}The Multistep Relation}{68}{subsection.4.3.4}%
\contentsline {subsection}{\numberline {4.3.5}The Big Step Relation}{68}{subsection.4.3.5}%
\contentsline {subsubsection}{Big Step and Cache Expressions}{69}{section*.21}%
\contentsline {subsection}{\numberline {4.3.6}User-Level Expressions}{69}{subsection.4.3.6}%
\contentsline {section}{\numberline {4.4}Conclusion}{70}{section.4.4}%
\contentsline {chapter}{\numberline {5}Higher Order Saiga}{71}{chapter.5}%
\contentsline {section}{\numberline {5.1}Higher Order Attributes}{71}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Expression Language}{72}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Type Rules}{73}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Semantic Rules}{73}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}Tree Optimisation Example}{74}{section.5.2}%
\contentsline {section}{\numberline {5.3}Discussion}{80}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}User-Level Expressions}{80}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Intrinsic Attributes on Higher Order Nodes}{81}{subsection.5.3.2}%
\contentsline {subsection}{\numberline {5.3.3}Building Initial Trees}{82}{subsection.5.3.3}%
\contentsline {subsubsection}{Tree Construction and Parsing}{83}{section*.22}%
\contentsline {subsubsection}{Forcing Full Construction}{83}{section*.23}%
\contentsline {subsection}{\numberline {5.3.4}Forwarding}{84}{subsection.5.3.4}%
\contentsline {subsection}{\numberline {5.3.5}The Multistep Relation}{85}{subsection.5.3.5}%
\contentsline {subsection}{\numberline {5.3.6}The Big Step Relation}{85}{subsection.5.3.6}%
\contentsline {chapter}{\numberline {6}Metatheoretic Properties}{87}{chapter.6}%
\contentsline {section}{\numberline {6.1}Axioms}{87}{section.6.1}%
\contentsline {section}{\numberline {6.2}Values Can Not Step}{88}{section.6.2}%
\contentsline {section}{\numberline {6.3}Step Determinism}{88}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Proof for the Core Calculus}{88}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}Proof for the Extended Calculus}{90}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Proof for the Higher Order Calculus}{92}{subsection.6.3.3}%
\contentsline {section}{\numberline {6.4}Type Determinism}{93}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Proof for the Core Calculus}{93}{subsection.6.4.1}%
\contentsline {subsection}{\numberline {6.4.2}Proof for the Extended Calculus}{95}{subsection.6.4.2}%
\contentsline {subsection}{\numberline {6.4.3}Proof for the Higher Order Calculus}{96}{subsection.6.4.3}%
\contentsline {section}{\numberline {6.5}Type Preservation over the Step Relation}{96}{section.6.5}%
\contentsline {subsection}{\numberline {6.5.1}Proof for the Core Calculus}{97}{subsection.6.5.1}%
\contentsline {subsection}{\numberline {6.5.2}Proof for the Extended Calculus}{99}{subsection.6.5.2}%
\contentsline {subsection}{\numberline {6.5.3}Proof for the Higher Order Calculus}{101}{subsection.6.5.3}%
\contentsline {section}{\numberline {6.6}Progress}{101}{section.6.6}%
\contentsline {subsection}{\numberline {6.6.1}Proof for the Core Calculus}{102}{subsection.6.6.1}%
\contentsline {subsection}{\numberline {6.6.2}Proof for the Extended Calculus}{103}{subsection.6.6.2}%
\contentsline {subsection}{\numberline {6.6.3}Proof for the Higher Order Calculus}{104}{subsection.6.6.3}%
\contentsline {section}{\numberline {6.7}Big Step Multistep Equivalence}{105}{section.6.7}%
\contentsline {subsection}{\numberline {6.7.1}Multistep Implies Big Step}{106}{subsection.6.7.1}%
\contentsline {subsection}{\numberline {6.7.2}Big Step Implies Multistep}{110}{subsection.6.7.2}%
\contentsline {subsection}{\numberline {6.7.3}Big Step Induction}{113}{subsection.6.7.3}%
\contentsline {section}{\numberline {6.8}Cache Irrelevance}{113}{section.6.8}%
\contentsline {section}{\numberline {6.9}Conclusion}{121}{section.6.9}%
\contentsline {chapter}{\numberline {7}Example}{123}{chapter.7}%
\contentsline {section}{\numberline {7.1}The Abstract Grammar}{124}{section.7.1}%
\contentsline {section}{\numberline {7.2}The Attributes}{124}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}The Kiama Implementation}{127}{subsection.7.2.1}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont env}}{127}{section*.24}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont decl}}{128}{section*.25}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type}}{128}{section*.26}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type} Aux Attributes}{128}{section*.27}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont fields}}{129}{section*.28}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont superClass}}{129}{section*.29}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont method}}{129}{section*.30}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont subTypeOf}}{129}{section*.31}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont thisFPDecl}}{129}{section*.32}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont objectClassDecl}}{130}{section*.33}%
\contentsline {subsubsection}{Aux Node Construction Attributes}{130}{section*.34}%
\contentsline {subsubsection}{Helper Attributes}{130}{section*.35}%
\contentsline {subsection}{\numberline {7.2.2}The JastAdd Implementation}{131}{subsection.7.2.2}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont decl}}{131}{section*.36}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type}}{132}{section*.37}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont subTypeOf}}{132}{section*.38}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookup}}{133}{section*.39}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont fields}}{133}{section*.40}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookupType}}{133}{section*.41}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookupMethod}}{133}{section*.42}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont remoteLookup}}{134}{section*.43}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont superClass}}{134}{section*.44}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont enclosingClassDecl}}{134}{section*.45}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont thisFPDecl}}{134}{section*.46}%
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont objectClassDecl}}{134}{section*.47}%
\contentsline {subsubsection}{Aux Node Construction Attributes}{134}{section*.48}%
\contentsline {subsubsection}{Helper Attributes}{135}{section*.49}%
\contentsline {section}{\numberline {7.3}Important Lemmas}{135}{section.7.3}%
\contentsline {section}{\numberline {7.4}Featherweight Java}{141}{section.7.4}%
\contentsline {subsection}{\numberline {7.4.1}Attributes in \ensuremath {A_{k\equiv j}}\xspace }{142}{subsection.7.4.1}%
\contentsline {subsection}{\numberline {7.4.2}The \text {\fontfamily {lcmss}\selectfont type} Attribute}{156}{subsection.7.4.2}%
\contentsline {subsubsection}{The EFld Case}{156}{section*.50}%
\contentsline {subsubsection}{The EIdn Case}{158}{section*.51}%
\contentsline {subsubsection}{The ECall Case}{163}{section*.52}%
\contentsline {subsubsection}{The ENew Case}{169}{section*.53}%
\contentsline {subsubsection}{The ECast Case}{171}{section*.54}%
\contentsline {subsubsection}{The \text {\fontfamily {lcmss}\selectfont type} Attribute Theorem}{173}{section*.55}%
\contentsline {section}{\numberline {7.5}Quantitative Analysis}{174}{section.7.5}%
\contentsline {subsection}{\numberline {7.5.1}Comparing Performance}{183}{subsection.7.5.1}%
\contentsline {chapter}{\numberline {8}Mechanisation}{185}{chapter.8}%
\contentsline {section}{\numberline {8.1}Configuration}{185}{section.8.1}%
\contentsline {section}{\numberline {8.2}Expression Language}{186}{section.8.2}%
\contentsline {section}{\numberline {8.3}Type Rules}{187}{section.8.3}%
\contentsline {section}{\numberline {8.4}The Context Function}{187}{section.8.4}%
\contentsline {subsection}{\numberline {8.4.1}Node Existence in Lean}{188}{subsection.8.4.1}%
\contentsline {section}{\numberline {8.5}The Step Relation}{188}{section.8.5}%
\contentsline {section}{\numberline {8.6}Metatheoretic Properties}{189}{section.8.6}%
\contentsline {subsection}{\numberline {8.6.1}Step Determinism}{190}{subsection.8.6.1}%
\contentsline {subsection}{\numberline {8.6.2}Type Determinism}{190}{subsection.8.6.2}%
\contentsline {subsection}{\numberline {8.6.3}Type Preservation}{190}{subsection.8.6.3}%
\contentsline {subsection}{\numberline {8.6.4}Progress}{190}{subsection.8.6.4}%
\contentsline {subsection}{\numberline {8.6.5}Big Step and Multistep}{191}{subsection.8.6.5}%
\contentsline {section}{\numberline {8.7}Cache Irrelevance}{191}{section.8.7}%
\contentsline {chapter}{\numberline {9}Conclusion}{193}{chapter.9}%
\contentsline {section}{\numberline {9.1}Evaluation of Contributions}{193}{section.9.1}%
\contentsline {section}{\numberline {9.2}Future Work}{196}{section.9.2}%
\contentsline {chapter}{\numberline {A}Appendix}{199}{appendix.A}%
\contentsline {section}{\numberline {A.1}Name Analysis Example}{199}{section.A.1}%
\contentsline {subsection}{\numberline {A.1.1}Kiama's Abstract Grammar for Featherweight Java}{199}{subsection.A.1.1}%
\contentsline {subsection}{\numberline {A.1.2}JastAdd's Abstract Grammar for Featherweight Java}{200}{subsection.A.1.2}%
\contentsline {section}{\numberline {A.2}Featherweight Java Code in Kiama and JastAdd}{201}{section.A.2}%
\contentsline {subsection}{\numberline {A.2.1}Kiama}{201}{subsection.A.2.1}%
\contentsline {subsection}{\numberline {A.2.2}JastAdd}{206}{subsection.A.2.2}%
\contentsline {chapter}{References}{211}{appendix*.56}%
