%\begin{savequote}[75mm]
%Get your shit together. Get it all together and put in a backpack. All your shit, so it's together. And if you gotta take it somewhere... take it somewhere, ya know? Take it to the shit store and sell it, or put it in a shit museum. I don't care what you do, you just gotta get it together. Get your shit togeter.
%\qauthor{Morty Smith (Rick and Morty, season 2 episode 7)}
%\end{savequote}

\begin{savequote}[55mm]
Here, I'm gonna do a thing.
\qauthor{ChocoTaco}
\end{savequote}

\chapter{Introduction}
  A programming language is an interface between a human and a computer.
  A human encodes some human-readable representation of a program into a source file, typically in text form.
  This source file is passed either to an interpreter, which performs some unknowable magic and executes the task described by the source file, or to a compiler which performs some similarly unknowable magic and produces an hieroglyphic version of that program, which somehow can be executed.
  Magic.
  
  \bigskip
  
  Of course this `magic' is knowable and, once known, is no longer magical.
  The structure and implementation of compilers is a well-researched area, with dozens of papers published every year at multiple international conferences (POPL, PLDI, SPLASH to name a few).
  Most computer science departments offer at least one undergraduate unit in language design.
  Once an \latin{ad hoc} endeavour, compilers are now built using various formalisms designed specifically for the task.
  Language workbenches~\cite{languageworkbenches-stateoftheart2013} bring all of these formalisms together into a single software suite designed to aid in language design and implementation.
  
  When teaching compiler design to undergraduates, we teach that there are three major stages to a compiler: structuring, translation, and encoding.
  Structuring involves lexical and syntactic analysis -- scanning, tokenising, parsing, and tree construction.
  This is the part of the compiler that reads some textual representation of a program and produces a tree representing that program.
  Translation involves semantic analysis -- understanding the semantics of the program tree, which includes type and name analysis.
  Translation also includes the construction of a target program tree -- the program tree that represents the compiled program.
  Encoding involves code generation and assembly -- the process of encoding a target program tree into a product that is ready for some machine (real or virtual) to execute.
  
  While every stage of a compiler is important, in this thesis we focus on \newterm{semantic analysis}; the process of analysing a source program tree to determine if it is valid, and to prepare extra semantic information for translation or execution, such as name bindings.
  Since semantic analysis is a specialised task, a specialised formalism called \newterm{attribute grammars} (AGs) has been developed to make semantic analysis easier and more consistent.
  AGs have been around since the late 60s~\cite{knuth68}, and are heavily researched.
  The formalism allows for the productions of a context-free grammar to be annotated with relational equations.
  
  Since the specification of an attribute grammar is concerned only with the relationships between nodes and values, and is not concerned with the order of evaluation, attribute grammars are considered a declarative formalism.
  A large portion of attribute grammar research, especially in the first few decades of its existence, was regarding \newterm{evaluation traversals}; generating algorithms that could traverse any conforming tree in a finite number of passes, guaranteeing that every attribute could be evaluated on every node in the tree.
  The strategy of analysing an attribute grammar and generating a finite evaluation traversal algorithm is called \newterm{static scheduling}.
  This process also involves rejecting attribute grammars that the scheduler cannot determine a schedule for -- whether no such schedule exists, or if it does not fit within the bounds set out by the scheduler.
  
  An alternative to static scheduling is \newterm{dynamic scheduling}, where an evaluator uses run-time information, such as the structure of the tree being decorated, to determine an evaluation order~\cite{jourdan84optimalrecursive}.
  This can take the form of on-demand evaluation, where attributes are only evaluated once their values are required.
  The disadvantage of this approach is that cyclic dependencies are often not discovered until run-time, but this also allows attribute grammar programs to be written outside of the various \newterm{attribute grammar classifications} that place restrictions on their specification.
  Dynamic scheduling also allows for partial evaluation -- with the rise of development environments working alongside compilers for instant error-reporting and diagnostics, it is valuable to be able to evaluate only the attributes that are requested; not every attribute in a tree all at once\footnote{Some static schedulers also allow for incremental evaluation, which can provide similar functionality for IDEs.}.
  
  Dynamic scheduling gives an \newterm{attribute grammar platform} the freedom to implement evaluation in a variety of new ways.
  These lifted restrictions also allow new attribute features and notations to exist, which would not integrate so neatly into statically scheduled platforms.
  \newterm{Reference attribute grammars}~\cite{hedin00reference}, for example, allow an attribute's value to depend on the value of an arbitrarily distant node, which can be selected during attribute evaluation.
  Reference attribute grammars can be implemented in statically scheduled attribute grammar systems~\cite{boyland05remote}, but exist more naturally in a dynamically scheduled system.
  
  The freedom provided by dynamic scheduling has led to more variety in attribute grammar platforms.
  Some platforms are deeply embedded in a general purpose programming language, as in the instance of Kiama~\cite{kiama}.
  Some platforms stand alone and parse an attribute grammar specification written in a specialised language, as in the instance of Silver~\cite{silver}.
  This variety is interesting, as two platforms that provide similar functionality can be expressed in vastly different ways, allow different notation possibilities, and implement different particular evaluation strategies.
  
\bigskip

  The problem created by this variety of approaches is that the essence of dynamically schedule attribute grammar evaluation is obfuscated by its many implementations.
  It is difficult to reason about any attribute grammar algorithm without implementing it in some attribute grammar platform, and being bound to the semantics of that platform's particular implementation, which may differ significantly to those of another platform.
  
\section{Contributions}\label{sect-intro-contributions}
  
  In this thesis we present a calculus that captures what is common between dynamically scheduled attribute grammar evaluators.
  We present a strategy for defining attributes that is flexible enough to mimic a variety of notations while exhibiting consistent evaluation semantics.
  Our calculus is specific to the domain of dynamically scheduled attribute grammar evaluation but general enough to represent all common notations and behaviours in the domain, and simple enough to allow proofs of evaluation to be easily expressed.
  
\newcommand{\contribemph}[1]{\bt{#1}}
  Specifically, the contributions of this thesis are as follows.
\begin{enumerate}
  \item A \contribemph{calculus} which captures the fundamental semantics of dynamically-scheduled attribute grammar evaluation, without being obscured by a general purpose language or by a particular attribute grammar platform's implementations.
  \item This calculus is defined in the form of an \contribemph{expression language}, \contribemph{type rules}, and a \contribemph{small-step operational semantics}.
  \item A key feature of the semantics presented is \contribemph{the context function}, which is a very flexible framework for defining attribute grammar equations that is notation agnostic.
  \item We begin with a core calculus which we extend by implementing some common and representative attribute grammar \contribemph{extensions}, including parameterised attributes, attribute caching, and higher order attributes.
  \item Our calculus represents a framework for reasoning about and comparing the behaviour of attribute grammar programs, aided by a set of \contribemph{metatheoretic properties}, for which we provide comprehensive proofs.
  \item The calculus itself, as well as proofs for the majority of these metatheoretic properties, are \contribemph{mechanised} in Lean~\cite{de15lean}.
  \item We \contribemph{demonstrate the utility} of these techniques through analysis of a real-world scale problem: comparing two different approaches to name and type analysis for Featherweight Java~\cite{igarashi01fwjgj}, translated directly from implementations in two different real-world attribute grammar platforms.
  \item This comparison is primarily a proof that two particular attributes always evaluate to the same value, but we also demonstrate Saiga's facility for \contribemph{quantitative analysis} by comparing the number of evaluation steps taken for a particular computation.
\end{enumerate}

  
    
\section{Outline}\label{sect-intro-outline}
  This thesis is organised as follows: first, in \cref{chap-background}, we discuss attribute grammars in general, including their history, and review some modern attribute grammar platforms.
  We examine common attribute grammar extensions, giving examples of how these extensions can be used in Silver, JastAdd, and Kiama, three modern dynamically scheduled attribute grammar platforms.
  The extensions we focus on are reference attributes, parameterised attributes, attribute caching, higher order attributes, and attribute forwarding.
  We briefly discuss circular and collection attributes.
  
  In \cref{chap-saiga} we present Saiga, our calculus for dynamically scheduled attribute grammar evaluation.
  Built on top of a simple functional calculus, we present an expression language, type rules, and a step relation that describes small step operational semantics.
  We discuss the particulars of Saiga's implementation and the design decisions behind the calculus, and present an example that models name analysis for a simple language.
  
  In \cref{chap-saiga-extended} we show how Saiga can be extended with parameterised attributes and attribute caching.
  We explore motivating examples, presenting an alternative approach to name analysis, and showing how caching can impact the performance of an evaluation.
  In \cref{chap-saiga-ho} we show how higher order attributes can be incorporated into the calculus, and how Saiga's flexible strategy for defining attribute equations give attribute forwarding for free.
  
  \cref{chap-meta} is concerned with proving properties of our calculus, including simple properties such as determinism and progress, as well as more involved properties such as cache irrelevance.
  In \cref{chap-example} we present an example of Saiga's use in analysing real-world attribute grammar programs by translating two different implementations of name and type analysis for Featherweight Java into Saiga specifications and proving their outputs to be equivalent.
  These specifications were taken as direct translations from Kiama and JastAdd programs written by the platform maintainers, and each use a different approach to name analysis as well as a different notational standard for defining their attributes.
  Saiga's ability to capture both programs -- with their distinct individual notations -- into the same low-level formalism demonstrates the strength of our approach.
  We also demonstrate Saiga's ability to perform quantitative analysis by comparing the number of evaluation steps taken by each approach.
  
  In \cref{chap-mechanisation} we briefly discuss the mechanisation of Saiga in Lean, and how the task of mechanising our calculus acted as a useful sanity check for many of our theorems.
  We demonstrate the strategies used to encode our semantics, and discuss the challenges that come with mechanising our particular approach to equation selection and higher order evaluation.
  In \cref{chap-conclusion} we summarise our findings and discuss directions for future work.
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  