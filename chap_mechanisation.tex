\begin{savequote}[75mm]
Those beer nerds are fucking merciless.
\qauthor{Anthony Bourdain}
\end{savequote}


\chapter{Mechanisation}\label{chap-mechanisation}
  From the beginning of Saiga's development, the on-paper calculus has developed alongside a mechanised counterpart.
  In the early stages we mechanised our calculus in Coq~\cite{barras97coq}, as mentioned in our first published work on Saiga~\cite{buckley17saiga}.
  In 2018 we moved from Coq to Lean~\cite{de15lean}, as we had run into some difficulties with the efficiency of proof automation in Coq.
  
  Saiga's mechanisation is not one of our major contributions; we mechanise to sanity-check our on-paper semantics and proofs.
  However, our approach and our results using Lean may be interesting to the reader, so they are presented here.
  
  We will not present the full code of our mechanisation in this thesis, as we have produced many thousands of lines of definitions and proof scripts.
  The full code of our mechanisation can be found at \mbox{\tt{\url{https://bitbucket.org/scottbuckley/saigalean}}}.
  The code referenced in \cref{sect-mech-config,sect-mech-exp,sect-mech-type,sect-mech-ctx,sect-mech-step} is defined in the file \tt{saiga.lean}.
  
\section{Configuration}\label{sect-mech-config}
  The first thing we define in our mechanised semantics is a \it{configuration}: this is a set of variables that make up an evaluation context.
  We call this configuration \tt{saigaconfig}, which is defined in the Lean snippet shown below.
  
\makeatletter
\def\addToLiterate#1{\edef\lst@literate{\unexpanded\expandafter{\lst@literate}\unexpanded{#1}}}
\lst@Key{literate}{}{\addToLiterate{#1}}
\makeatother

\newcommand{\bigstep}{\ \ensuremath{»»}\ }

\lstset{
  escapeinside={(*}{*)},
  literate={-}{-}1,
  literate={\\tau}{$\tau$}1,
  literate={\\rho}{$\rho$}1,
  literate={\\forall}{$\forall$}1,
  literate={\\times}{$\times$}1,
  literate={\\lambda}{$\lambda$}1,
  literate={\\vdash}{$\vdash$}1,
  literate={\\vDash}{$\vDash$}1,
  literate={\\longrightarrow}{$\longrightarrow$}2,
  literate={\\Longrightarrow}{$\Longrightarrow$}2,
  literate={\\and}{$\wedge$}1,
  literate={\\or}{$\vee$}1,
  literate={\\exists}{$\exists$}1,
  numbers=left
}

\begin{lstlisting}
structure saigaconfig :=
  (A:  Type)
  (\tau \rho:  A -> Type)
  [Adec: decidable_eq A]
  [\tauinh: \forall (a:A), inhabited \tau a)]
  [\rhodec: \forall (a:A), decidable_eq (\rho a)]

variable cfg: saigaconfig
\end{lstlisting}
  
  We also declare the variable \tt{cfg}, which is a global variable of type \tt{saigaconfig}.
  What this means is that we assume some \tt{cfg} of type \tt{saigaconfig} exists.
  This way we can write proofs that assume a \tt{saigaconfig} exists, and Lean will automatically generalise these proofs over \tt{saigaconfig}.
  The contents of a \tt{saigaconfig} are simple: line 2 specifies that there is some type representing the set of attributes \A.
  Line 3 specifies that there are two functions $\tau$ and $\rho$ which return a type for every member of \A.
  Line 4 specifies that there is decidable equality between members of \A.
  Lines 5 and 6 specify that the return types of $\tau$ and $\rho$ are inhabited and have decidable equality, respectively.
  
\section{Expression Language}\label{sect-mech-exp}
  We define the expression language of Saiga as an inductive type.
  
\begin{lstlisting}
structure ap :=  mk :: (attr:cfg.A) (param:cfg.\rho attr)

inductive exp
| eVal    {T:Type} (v:T):                             exp
| eCond   (eC eT eF:exp):                             exp
| eApp    (eF eP: exp):                               exp
| eAttr   (eN: exp) (a:cfg.A) (eP:exp):               exp
| eCache  (n:node) (a:cfg.A) (p:cfg.\rho a) (e:exp):     exp
| eMk     (fl:node -> list (exp \times ap cfg)):           exp
\end{lstlisting}

  Lines 3-9 give the definition of the \tt{exp} type, which matches the expression grammar given in \cref{sect-ho-grammar}.
  This type describes the \emph{abstract} syntax of a Saiga expression, not the \emph{concrete} syntax, so parsing strings such as ``IF'' and ``THEN'' are not included here.
  Each of the constructors of \tt{exp} on lines 4-9 clearly implement one of the productions of the Saiga expression grammar.
  The type \tt{ap} is a dependent tuple of an attribute and an associated parameter value, used in the definition of \tt{eMk}.
  In our mechanisation we do not force the attribute written to a higher order node to be parameterless, as expressing $\A_u$ would be more complicated in Lean than simply allowing parameterised attributes to be written.
  Therefore our definition of a \mk expression differs slightly in our mechanisation to the version presented in \cref{chap-saiga-ho}.
  
  We define notations for each of the constructors of \tt{exp} to allow expressions to be written as follows.
\begin{itemize}
  \item $\tt{\{\{}v\tt{\}\}}$ in Lean represents $\val{v}$.
  \item $\tt{IFF } e_1 \tt{ THEN } e_2 \tt{ ELSE } e_3$ in Lean represents $\ifff e_1 \thenn e_2 \elsee e_3$. We use \tt{IFF} instead of \tt{IF} to avoid conflict with Lean keywords.
  \item $e_1 \tt{ OF } e_2$ in Lean represents $e_1\with{e_2}$. The parameter syntax is more difficult to represent in Lean than an infix.
  \item $e_1 \tt{ DOT } a \tt{ WITH } e_2$ in Lean represents $e_1\dott a\with{e_2}$. Again we prefer an infix notation in Lean. We also define $e_1 \tt{ DOT } a$ in Lean to represent $e_1\dott a$, allowing the unit value to be inserted transparently.
  \item $n \tt{ / } a \tt{ / } p \tt{ ;= } e$ in Lean represents $n\dott a\with{p}\caches e$. Overloading the \tt{DOT}, \tt{WITH} syntax would be too difficult to achieve in Lean.
  \item $\tt{MK } f_l$ in Lean represents $\mk f_l$. We usually use Lean's inbuilt lambda syntax to define the function $f_l$.
\end{itemize}

\section{Type Rules}\label{sect-mech-type}
  We define type inference rules in Lean as an inductive proposition.
  We show a snippet of this definition below.
\begin{lstlisting}
inductive expType {cfg} : exp cfg -> Type -> Prop
| eVal {T:Type} {v:T}:
      expType {{v}} T
| eAttr {eN eP:exp cfg} {a:cfg.A}
    (nt: expType eN (node))
    (pt: expType eP (cfg.\rho a)):
      expType (eN DOT a WITH eP) (cfg.\tau a)  
\end{lstlisting}

  The inductive proposition given above represents a relation between values in \tt{exp} and values in \tt{Type} (types in Lean are how we implement \T).
  We can see from the constructor on lines 2-3 that the type of a value expression is taken directly from its contained value, as in \typeval in \cref{sect-ho-typerules}.
  Similarly, lines 4-7 define types for attribution expressions using $\tau$, enforcing subexpression types to \N and $\rho(a)$.
  Type inference rules are given for all other expression types, matching the rules presented in \cref{sect-ho-typerules}.
  
\bigskip

  In earlier versions of our mechanisation, we built expression types into the constructors of \tt{exp}, giving \tt{exp} a type parameter.
  For example, the type \tt{exp bool} would represent an expression of type \tt{bool}.
  This was easy to express, and allowed Lean's type system to perform all type checking on our expression language.
  Further, we were able to create a context function type that always returns an expression of a type that matches the input attribute.
  However, Lean's tactics would often fail when trying to perform induction on these dependent inductive types.
  This issue was significant enough to cause us to abandon the approach entirely and resort to specifying expression types using a relation as described above.
  
  
\section{The Context Function}\label{sect-mech-ctx}
  We define a context function as a simple (dependent) function in Lean, which takes as parameters a node, an attribute, and a parameter value, returning an expression.
\begin{lstlisting}[numbers=none]
def context := \forall (n:node) (a:cfg.A) (p:cfg.\rho a), exp cfg  
\end{lstlisting}

  We then implement the $\oplus$ operator in the function \tt{emptyC} shown below.
\begin{lstlisting}
def extendC (n:node) (a:cfg.A) (p:cfg.\rho a) (v:cfg.\tau a)
            (c:context cfg) : context cfg :=
  \lambda n' a' p',
    bite (napmatch cfg n n' a a' p p') 
    ({{v}})
    (c n' a' p')  
\end{lstlisting}

  The inputs to \tt{extendC} are a node, an attribute, and a parameter and value that are typed according to $\tau$ and $\rho$, as well as a context function.
  The output is a function that matches against the first three inputs, returning either the value \tt{v} given, or deferring to the original context function \tt{c}.
  
  We similarly implement \tt{extendE}, which modifies a context function's output with an expression (rather than specifically a value expression).
  We then define \tt{writeAL}, which repeatedly applies \tt{extendE}, implementing the $\otimes$ operator.

  
\subsection{Node Existence in Lean}
  Instead of implementing node existence in our Lean mechanisation using the methods discussed in \cref{chap-saiga-ho}, we instead define a type \tt{ctxN}, which contains both a context function and a list of nodes, such that node existence now depends on a \tt{ctxN}, rather than just a context function.
  This makes reasoning about node existence simpler during proofs, but means that proofs around higher order attribution differ slightly between our mechanised and on-paper proofs.
  
  Higher order semantics were the hardest part of Saiga to mechanise, and not all of our mechanised proofs around higher order semantics are complete.
  Specifically, our mechanised proof for cache irrelevance has some holes around our higher order semantics.


\section{The Step Relation}\label{sect-mech-step}
  As with type inference rules, we define the step relation in Lean as an inductive proposition, as shown below.
  The \tt{step} proposition takes a context function (specifically a \tt{ctxN}, which represents a context function and a list of nodes), an initial expression, a second context function and expression.
  The proposition \tt{step c1 e2 c2 e2} is equivalent to $\tt{c1} |- \tt{e1} \steps \tt{c2} |- \tt{e2}$ in the notation we have introduced in this thesis.
  In Lean, we implement the notation $\tt{c1} \vDash \tt{e1} \longrightarrow \tt{c2} \vDash \tt{e2}$, allowing us to express the same proposition in a similar fashion.
\begin{lstlisting}
inductive step {cfg}:ctxN cfg -> exp cfg -> ctxN cfg -> exp cfg -> Prop
| condTrue {ctx:ctxN _} {eT eF:exp cfg}:
  step ctx (IFF {{tt}} THEN eT ELSE eF) ctx eT

| appApp {T1 T2:Type} {ctx:ctxN cfg} {f:(T1 -> T2)} {p:T1}:
  step ctx ({{f}} OF {{p}}) ctx {{f p}}
  
| attrFetchCached {ctx:ctxN _} {a:cfg.A} {n:node} {p:cfg.\rho a}
  (nv: notvalue (ctx.c n a p)):
  step ctx ({{n}} DOT a WITH {{p}}) ctx (n/a/p ;= (ctx.c n a p))
  
| cacheWrite {ctx:ctxN _} {a:cfg.A} {v:cfg.\tau a} {n:node} {p:cfg.\rho a}:
  step ctx (n / a / p ;= {{v}}) (@extendCN cfg n a p v ctx) {{v}}
\end{lstlisting}

  The \tt{step} relation in Lean has 13 constructors, matching the 13 rules shown in \cref{sect-ho-semantics}.
  Above, we have shown only four of these constructors.
  \tt{condTrue} implements the simple \condtrue step.
  \tt{appApp} implements the \funapp step, where the syntax \tt{f p} indicates function application using Lean's function language (the underlying system in our Lean implementation is, of course, Lean).
  \tt{attrFetchCached} implements the \attrfetchcached rule, requiring the \tt{notvalue} predicate defined elsewhere.
  Finally, \tt{cacheWrite} implements the \cachewrite rule, using \tt{extendCN} to update a context function.
  
  Similarly, we define the multistep and big step relations as the inductive propositions \tt{multistep} and \tt{bigstep}.

\section{Metatheoretic Properties}\label{sect-mech-meta}
  All of the code referenced in the preceding sections of this chapter is contained in \tt{saiga.lean} in our repository.
  In \tt{saiga\_lemmas.lean} we define many lemmas that are used to aid proofs in this and later sections, none of which are interesting enough to share here.
  Built upon these lemmas are the metatheoretic theorems contained in \tt{saiga\_metatheoretic.lean}.
  
  To give the reader some understanding the process we undertake to write proofs about Saiga, we will walk through the first (and simplest) methatheoretic theorem \tt{value.nostep}, which states that a value expression can not be on the left of a step relation.
  We first present the entirety of this theorem below, including its proof.
\begin{lstlisting}
theorem value.nostep {cfg:saigaconfig}:
  \forall {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
    (c1 \vDash {{v}} \longrightarrow c2 \vDash e) -> false
:= begin
  intros c1 c2 e T v hs,
  cases hs,
end  
\end{lstlisting}

  The crux of the definition of this theorem is given on line 3, which uses our Lean notation for the single step relation and value expressions to say that a relation with a value expression on its left implies \tt{false}.
  \tt{false} is the proposition in Lean that can never occur, so saying that something implies \tt{false} is saying that something can not occur.
  Before this, line 2 says that this theorem holds for any values of \tt{c1} and \tt{c2} (which are context functions), \tt{e} (which is an expression), and \tt{v} (which is of some type \tt{T}).
  
  The proof for \tt{value.nostep} is given between the keywords \tt{begin} and \tt{end}, on lines 5 and 6.
  Line 5 introduces the variables defined on line 2 and the step relation on line 3 as named hypotheses.
  Lean's proof state after line 5 is given below.
\begin{lstlisting}
1 goal
cfg : saigaconfig,
c1 c2 : ctxN cfg,
e : exp cfg,
T : Type,
v : T,
hs : c1 \vDash {{v}} \longrightarrow c2 \vDash e
\vdash false  
\end{lstlisting}
  In the above proof state, line 1 indicates that there is only one goal to solve.
  Lines 2 to 6 describe the six givens, with their names, and line 7 gives the name \tt{hs} to the hypothesis describing the step relation.
  Line 8 is the single goal of \tt{false}.
  
  This proof is completed using the \tt{cases} tactic on the hypothesis \tt{hs}, as shown on line 6 of the first code listing of this section.
  The \tt{cases} tactic replaces the existing goal with one goal for each of the rules that could have created the proposition in question.
  As there are exactly zero ways that \tt{hs} could have been created, this tactic immediately satisfies the theorem by removing all goals.
  After applying this tactic, Lean reports \tt{goals accomplished}, indicating that the theorem is proven.
  
  We define and prove a number of related theorems.
  For each of the following theorems, we will show only the definition of the theorem, not the body of the proof.
  
\subsection{Step Determinism}
  Determinism of the step relation, as discussed in \cref{sect-meta-stepdeterminism}, is specified as follows, and its full proof is shown in \tt{saiga\_metatheoretic.lean}.
\begin{lstlisting}
theorem step.determinism {cfg:saigaconfig}:
  \forall {c1 c2 c3:ctxN cfg} {e1 e2 e3:exp cfg},
    (c1 \vDash e1 \longrightarrow c2 \vDash e2) ->
    (c1 \vDash e1 \longrightarrow c3 \vDash e3) ->
    e3 = e2 \and c3 = c2  
\end{lstlisting}


\subsection{Type Determinism}
  Determinism of the type relation, as discussed in \cref{sect-meta-typedeterminism}, is specified as follows, and its full proof is shown in \tt{saiga\_metatheoretic.lean}.
\begin{lstlisting}
theorem type.determinism {cfg:saigaconfig}:
  \forall {e:exp cfg} {T1 T2:Type}
    (t1: expType e T1)
    (t2: expType e T2),
    T2 = T1  
\end{lstlisting}

\subsection{Type Preservation}
  Preservation of the type relation over the step relation, as discussed in \cref{sect-meta-typepreservation}, is specified as follows, and its full proof is shown in \tt{saiga\_metatheoretic.lean}.
\begin{lstlisting}
theorem step.type_preservation {cfg:saigaconfig}:
  \forall {c1 c2:ctxN cfg} {e1 e2:exp cfg} {T:Type}
    (hts: type_safe c1)
    (ht: expType e1 T)
    (hs: c1 \vDash e1 \longrightarrow c2 \vDash e2),
    expType e2 T  
\end{lstlisting}

\subsection{Progress}
  Progress of the step relation, as discussed in \cref{sect-meta-progress}, is specified as follows, and its full proof is shown in \tt{saiga\_metatheoretic.lean}.
\begin{lstlisting}
theorem step.progress {cfg:saigaconfig}:
  \forall {c1:ctxN cfg} {e1:exp cfg} {T:Type}
    (hts: type_safe c1)
    (ht: expType e1 T),
    (\exists c2 e2, c1 \vDash e1 \longrightarrow c2 \vDash e2) \or value e1 \or cont_mk e1
\end{lstlisting}


\subsection{Big Step and Multistep}
  We represent the big step relation (\tt{bigstep} in Lean) using the notation $\tt{c1} \vDash \tt{e} \bigstep \tt{c2} \vDash \tt{v}$, and the multistep relation (\tt{multistep} in Lean) using the notation $\tt{c1} \vDash \tt{e} \Longrightarrow \tt{c2} \vDash \tt{v}$.
  Equivalence of the big step and multistep relations, as discussed in \cref{sect-meta-multibig}, is specified as shown in the following two snippets, and their full proofs are shown in \tt{saiga\_metatheoretic.lean}.
\begin{lstlisting}
theorem multistep.bigstep {cfg:saigaconfig}:
  \forall {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T}
    (hs: c1 \vDash e \Longrightarrow c2 \vDash v),
    (c1 \vDash e (*$»»$*) c2 \vDash v)
\end{lstlisting}

\begin{lstlisting}
theorem bigstep.multistep {cfg:saigaconfig}:
  \forall {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T}
    (hs: c1 \vDash e (*$»»$*) c2 \vDash v),
    (c1 \vDash e \Longrightarrow c2 \vDash v)
\end{lstlisting}

  The approach to proving these theorems in Lean is the same as our proofs of \cref{thm-multistepbigstep,thm-bigstepmultistep} in \cref{chap-meta}: we prove a lemma about a single step followed by a big step (\cref{lem-stepbigstep}) to prove \tt{multistep.bigstep}, and we prove a number of similar lemmas (\cref{lem-multisteprec-cond,lem-multisteprec-appf,lem-multisteprec-appp,lem-multisteprec-attrn,lem-multisteprec-attrp,lem-multisteprec-cache}) to prove \tt{bigstep.multistep}.
  
\section{Cache Irrelevance}\label{sect-mech-cacheirrel}
  As in \cref{sect-meta-cacheirrel}, our mechanisation approaches cache irrelevance by defining an equivalence relation between context functions and proving that big step evaluation will always produce an output context function that is equivalent to the input context function.
  Our proofs are found in the Lean files \tt{saiga\_irrelprep.lean} and \tt{saiga\_irrel.lean}.
  We state and prove the same series of lemmas we present in \cref{sect-meta-cacheirrel}, which build upon one another to eventually provide a proof for ``big step equivalence''.
  
  We mechanise \cref{axiom-function-type-equal}, as well as a number of axioms to implement ``node existence'' behaviour.
  For the higher order section of our proof, we also rely on some properties that we take as axioms.
  Our mechanisation for cache irrelevance is not complete, as we discuss in \cref{sect-conclusion-futurework}.
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  