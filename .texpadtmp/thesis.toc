\contentsline {chapter}{Abstract}{v}{chapter*.1}
\contentsline {chapter}{Contents}{vii}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Contributions}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Outline}{5}{section.1.2}
\contentsline {chapter}{\numberline {2}Background}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Early Attribute Grammars}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Statically Scheduled Attribute Grammar Categories}{10}{subsection.2.1.1}
\contentsline {subsubsection}{Purely Synthesised Attribute Grammars}{10}{section*.3}
\contentsline {subsubsection}{Non-Circular Attribute Grammars}{10}{section*.4}
\contentsline {subsubsection}{Strongly (or Absolutely) Non-Circular Attribute Grammars}{10}{section*.5}
\contentsline {subsubsection}{Ordered Attribute Grammars}{11}{section*.6}
\contentsline {subsubsection}{Other Categories}{12}{section*.7}
\contentsline {section}{\numberline {2.2}Dynamically Scheduled Attribute Grammars}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Attribute Grammar Notations}{13}{subsection.2.2.1}
\contentsline {subsubsection}{Silver}{14}{section*.8}
\contentsline {subsubsection}{JastAdd}{15}{section*.9}
\contentsline {subsubsection}{Kiama}{17}{section*.10}
\contentsline {section}{\numberline {2.3}Attribute Grammar Extensions}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Reference Attribute Grammars}{19}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Parameterised Attribute Grammars}{22}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Attribute Caching}{24}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Higher Order Attribute Grammars}{27}{subsection.2.3.4}
\contentsline {subsubsection}{Higher Order Nodes in the Existing Tree}{28}{section*.11}
\contentsline {subsection}{\numberline {2.3.5}Attribute Forwarding}{29}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Circular Attribute Grammars}{31}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}Collection attributes}{32}{subsection.2.3.7}
\contentsline {section}{\numberline {2.4}Previous Attempts}{35}{section.2.4}
\contentsline {chapter}{\numberline {3}Saiga}{37}{chapter.3}
\contentsline {section}{\numberline {3.1}Calculus}{38}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}An Underlying System}{38}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}All Types}{40}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Expression Language}{40}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Type Rules}{41}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Semantic Rules}{41}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Intrinsic, Structural, and Extrinsic Attributes}{43}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Simple Example}{44}{subsection.3.1.7}
\contentsline {section}{\numberline {3.2}Discussion}{47}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Equation Selection and the Context Function}{47}{subsection.3.2.1}
\contentsline {subsubsection}{Demonstration}{49}{section*.12}
\contentsline {subsection}{\numberline {3.2.2}Equation Selection and Node Types}{51}{subsection.3.2.2}
\contentsline {subsubsection}{Symbol-Based Equation Selection}{52}{section*.13}
\contentsline {subsubsection}{Production-Based Equation Selection}{53}{section*.14}
\contentsline {subsubsection}{Selector Notations}{55}{section*.15}
\contentsline {subsubsection}{Selector Example}{58}{section*.16}
\contentsline {subsubsection}{Invalid Context Functions and Non-Terminating Evaluation}{59}{section*.17}
\contentsline {subsection}{\numberline {3.2.3}Conditional Expressions}{60}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Multiple-Parameter Functions}{61}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Tuples}{62}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Functions Returning Expressions}{63}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Option Types and the Null Node}{64}{subsection.3.2.7}
\contentsline {subsection}{\numberline {3.2.8}The Multistep Relation}{65}{subsection.3.2.8}
\contentsline {subsection}{\numberline {3.2.9}The Big Step Relation}{66}{subsection.3.2.9}
\contentsline {section}{\numberline {3.3}Name Analysis Example}{66}{section.3.3}
\contentsline {section}{\numberline {3.4}Conclusion}{70}{section.3.4}
\contentsline {chapter}{\numberline {4}Saiga Extended}{71}{chapter.4}
\contentsline {section}{\numberline {4.1}Parameterised Attributes}{72}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Expression Language}{72}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}All Types}{73}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Type Rules}{73}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Semantic Rules}{74}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Name Analysis Example Revisited}{74}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Attribute Caching}{77}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Expression Language}{78}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Type Rules}{78}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Semantic Rules}{79}{subsection.4.2.3}
\contentsline {subsubsection}{A Changing Context Function}{79}{section*.18}
\contentsline {subsubsection}{Caching Rules}{80}{section*.19}
\contentsline {subsubsection}{Two Attribution Rules}{81}{section*.20}
\contentsline {subsection}{\numberline {4.2.4}Caching Example}{81}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Discussion}{84}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Non-Parameterised Attributes}{84}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Attributes Returning Functions}{85}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Re-Caching Values and Simplicity}{86}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}The Multistep Relation}{88}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}The Big Step Relation}{88}{subsection.4.3.5}
\contentsline {subsubsection}{Big Step and Cache Expressions}{89}{section*.21}
\contentsline {subsection}{\numberline {4.3.6}User-Level Expressions}{90}{subsection.4.3.6}
\contentsline {section}{\numberline {4.4}Conclusion}{90}{section.4.4}
\contentsline {chapter}{\numberline {5}Higher Order Saiga}{93}{chapter.5}
\contentsline {section}{\numberline {5.1}Higher Order Attributes}{93}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Expression Language}{95}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Type Rules}{96}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Semantic Rules}{96}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Tree Optimisation Example}{97}{section.5.2}
\contentsline {section}{\numberline {5.3}Discussion}{105}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}User-Level Expressions}{105}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Intrinsic Attributes on Higher Order Nodes}{106}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Building Initial Trees}{107}{subsection.5.3.3}
\contentsline {subsubsection}{Tree Construction and Parsing}{109}{section*.22}
\contentsline {subsubsection}{Forcing Full Construction}{109}{section*.23}
\contentsline {subsection}{\numberline {5.3.4}Forwarding}{110}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}The Multistep Relation}{112}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}The Big Step Relation}{112}{subsection.5.3.6}
\contentsline {chapter}{\numberline {6}Metatheoretic Properties}{115}{chapter.6}
\contentsline {section}{\numberline {6.1}Axioms}{116}{section.6.1}
\contentsline {section}{\numberline {6.2}Values Can Not Step}{116}{section.6.2}
\contentsline {section}{\numberline {6.3}Step Determinism}{117}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Proof for the Core Calculus}{117}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Proof for the Extended Calculus}{119}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Proof for the Higher Order Calculus}{121}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Type Determinism}{122}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Proof for the Core Calculus}{123}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Proof for the Extended Calculus}{125}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Proof for the Higher Order Calculus}{126}{subsection.6.4.3}
\contentsline {section}{\numberline {6.5}Type Preservation over the Step Relation}{127}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Proof for the Core Calculus}{128}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Proof for the Extended Calculus}{130}{subsection.6.5.2}
\contentsline {subsection}{\numberline {6.5.3}Proof for the Higher Order Calculus}{132}{subsection.6.5.3}
\contentsline {section}{\numberline {6.6}Progress}{134}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}Proof for the Core Calculus}{134}{subsection.6.6.1}
\contentsline {subsection}{\numberline {6.6.2}Proof for the Extended Calculus}{135}{subsection.6.6.2}
\contentsline {subsection}{\numberline {6.6.3}Proof for the Higher Order Calculus}{137}{subsection.6.6.3}
\contentsline {section}{\numberline {6.7}Big Step Multistep Equivalence}{139}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}Multistep Implies Big Step}{139}{subsection.6.7.1}
\contentsline {subsection}{\numberline {6.7.2}Big Step Implies Multistep}{144}{subsection.6.7.2}
\contentsline {subsection}{\numberline {6.7.3}Big Step Induction}{148}{subsection.6.7.3}
\contentsline {section}{\numberline {6.8}Cache Irrelevance}{149}{section.6.8}
\contentsline {section}{\numberline {6.9}Conclusion}{158}{section.6.9}
\contentsline {chapter}{\numberline {7}Example}{159}{chapter.7}
\contentsline {section}{\numberline {7.1}The Abstract Grammar}{160}{section.7.1}
\contentsline {section}{\numberline {7.2}The Attributes}{163}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}The Kiama Implementation}{163}{subsection.7.2.1}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont env}}{164}{section*.24}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont decl}}{165}{section*.25}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type}}{165}{section*.26}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type} Aux Attributes}{166}{section*.27}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont fields}}{166}{section*.28}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont superClass}}{167}{section*.29}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont method}}{167}{section*.30}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont subTypeOf}}{167}{section*.31}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont thisFPDecl}}{168}{section*.32}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont objectClassDecl}}{168}{section*.33}
\contentsline {subsubsection}{Aux Node Construction Attributes}{168}{section*.34}
\contentsline {subsubsection}{Helper Attributes}{169}{section*.35}
\contentsline {subsection}{\numberline {7.2.2}The JastAdd Implementation}{169}{subsection.7.2.2}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont decl}}{170}{section*.36}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont type}}{171}{section*.37}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont subTypeOf}}{172}{section*.38}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookup}}{172}{section*.39}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont fields}}{173}{section*.40}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookupType}}{173}{section*.41}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont lookupMethod}}{173}{section*.42}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont remoteLookup}}{174}{section*.43}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont superClass}}{174}{section*.44}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont enclosingClassDecl}}{174}{section*.45}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont thisFPDecl}}{174}{section*.46}
\contentsline {subsubsection}{\text {\fontfamily {lcmss}\selectfont objectClassDecl}}{175}{section*.47}
\contentsline {subsubsection}{Aux Node Construction Attributes}{175}{section*.48}
\contentsline {subsubsection}{Helper Attributes}{176}{section*.49}
\contentsline {section}{\numberline {7.3}Important Lemmas}{176}{section.7.3}
\contentsline {section}{\numberline {7.4}Featherweight Java}{182}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Attributes in \ensuremath {A_{k\equiv j}}\xspace }{183}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}The \text {\fontfamily {lcmss}\selectfont type} Attribute}{199}{subsection.7.4.2}
\contentsline {subsubsection}{The EFld Case}{200}{section*.50}
\contentsline {subsubsection}{The EIdn Case}{201}{section*.51}
\contentsline {subsubsection}{The ECall Case}{208}{section*.52}
\contentsline {subsubsection}{The ENew Case}{215}{section*.53}
\contentsline {subsubsection}{The ECast Case}{217}{section*.54}
\contentsline {subsubsection}{The \text {\fontfamily {lcmss}\selectfont type} Attribute Theorem}{219}{section*.55}
\contentsline {section}{\numberline {7.5}Quantitative Analysis}{220}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Comparing Performance}{231}{subsection.7.5.1}
\contentsline {chapter}{\numberline {8}Mechanisation}{233}{chapter.8}
\contentsline {section}{\numberline {8.1}Configuration}{234}{section.8.1}
\contentsline {section}{\numberline {8.2}Expression Language}{234}{section.8.2}
\contentsline {section}{\numberline {8.3}Type Rules}{235}{section.8.3}
\contentsline {section}{\numberline {8.4}The Context Function}{236}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}Node Existence in Lean}{237}{subsection.8.4.1}
\contentsline {section}{\numberline {8.5}The Step Relation}{237}{section.8.5}
\contentsline {section}{\numberline {8.6}Metatheoretic Properties}{238}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Step Determinism}{239}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Type Determinism}{240}{subsection.8.6.2}
\contentsline {subsection}{\numberline {8.6.3}Type Preservation}{240}{subsection.8.6.3}
\contentsline {subsection}{\numberline {8.6.4}Progress}{240}{subsection.8.6.4}
\contentsline {subsection}{\numberline {8.6.5}Big Step and Multistep}{240}{subsection.8.6.5}
\contentsline {section}{\numberline {8.7}Cache Irrelevance}{241}{section.8.7}
\contentsline {chapter}{\numberline {9}Conclusion}{243}{chapter.9}
\contentsline {section}{\numberline {9.1}Evaluation of Contributions}{244}{section.9.1}
\contentsline {section}{\numberline {9.2}Future Work}{247}{section.9.2}
\contentsline {chapter}{\numberline {A}Appendix}{249}{appendix.A}
\contentsline {section}{\numberline {A.1}Name Analysis Example}{249}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Kiama's Abstract Grammar for Featherweight Java}{249}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}JastAdd's Abstract Grammar for Featherweight Java}{251}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}Featherweight Java Code in Kiama and JastAdd}{251}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Kiama}{251}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}JastAdd}{257}{subsection.A.2.2}
\contentsline {chapter}{References}{263}{appendix*.56}
