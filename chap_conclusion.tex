

\begin{savequote}[65mm]
The other day upon the stair; \\
I met a man who wasn't there. \\
He wasn't there again today. \\
I wish that man would go away.
\qauthor{Paraphrased excerpt from Antigonish by William Hughes Mearns}
\end{savequote}


\chapter{Conclusion}\label{chap-conclusion}
  In this thesis we have presented a foundational semantics of dynamically scheduled attribute grammar evaluation, in the calculus we call Saiga.
  We began in \cref{chap-background} with a history of attribute grammars, and discussed the state of the art in dynamic evaluation, including discussion of a number of common extensions to attribute grammar platforms.
  We presented the core version of Saiga in \cref{chap-saiga}, extended this with parameterised attributes and caching in \cref{chap-saiga-extended}, and further extended our semantics to include higher order evaluation in \cref{chap-saiga-ho}.
  
  In \cref{chap-meta} we presented and proved a number of metatheoretic properties about Saiga, in its core, extended, and higher order forms.
  The most complex property to prove was cache irrelevance, which states that including caching operations (and higher order operations) does not affect the semantics of evaluation for any valid attribute grammar program.
  
  We demonstrated the utility of Saiga in \cref{chap-example}, where we used our calculus to formally compare the type analysis of two compilers for the same language, which were written using two different attribute grammar platforms and using two different approaches to name analysis.
  We were able to show that a key attribute would always evaluate to the same value for each approach, and we also compared the number of evaluation steps taken by each approach, finding their worst-case step count to be almost identical.

\section{Evaluation of Contributions}
  In \cref{sect-intro-contributions} we outlined the eight specific contributions of this thesis.
  Here we will repeat those contributions (in italics), and briefly discuss how and where each contribution was demonstrated. 
\newcommand{\quotecontrib}[1]{\it{#1}}
\begin{enumerate}
  \item \quotecontrib{A \contribemph{calculus} which captures the fundamental semantics of dynamically-scheduled attribute grammar evaluation, without being obscured by a general purpose language or by a particular attribute grammar platform's implementations.}
  \item[] We presented Saiga in \cref{chap-saiga,chap-saiga-extended,chap-saiga-ho}.
  Saiga models attribute grammar evaluation, focusing on the semantics of things like fetching attributes, writing to cache, and accessing basic function calls, without focusing on the details of those function calls (by deferring to the underlying system) or the details of the equation selection process (by deferring to the context function).
  The example in \cref{chap-example} demonstrated Saiga's ability to specify attributes in notations faithful to different real-world attribute grammar platforms, while being unconcerned with the particulars of either platform's equation selection process.
  
  \item \quotecontrib{This calculus is defined in the form of an \contribemph{expression language}, \contribemph{type rules}, and a \contribemph{small-step operational semantics}.}
  \item[] Saiga's expression language is presented in its core, extended, and higher order forms in \cref{sect-saiga-grammar,sect-ext-cache-saigagrammar,sect-ho-grammar}, its type rules in \cref{sect-saiga-typerules,sect-ext-cache-typerules,sect-ho-typerules}, and its operational semantics in \cref{sect-saiga-semantics,sect-ext-cache-semantics,sect-ho-semantics}.
  Each of these sets of specifications is simple in its design, and each variation is discussed in detail, explaining the design decisions that went into their specification, as well as presenting motivating examples to demonstrate their use.
  
  \item \quotecontrib{A key feature of the semantics presented is \contribemph{the context function}, which is a very flexible framework for defining attribute grammar equations that is notation agnostic.}
  \item[] The context function is defined for the core calculus in \cref{chap-saiga}, modified slightly in \cref{chap-saiga-extended} to allow for parameterisation, and left unchanged in \cref{chap-saiga-ho}.
  We define two simple methods of specifying a modified context function with the $\oplus$ and $\otimes$ operators, without specifying or relying on the inner workings of the function.
  We present a set of ``selector'' notations in \cref{sect-saiga-selectors}, which are a useful example of how a context function might be defined, but we do not rely on these notations at any point in our calculus -- the context function remains open to be defined arbitrarily, as long as it remains a mathematical function.
  
  \item \quotecontrib{We begin with a core calculus which we extend by implementing some common and representative attribute grammar \contribemph{extensions}, including parameterised attributes, attribute caching, and higher order attributes.}
  \item[] Parameterised attributes and attribute caching are presented in \cref{chap-saiga-extended}.
  Parameterisation is achieved by modifying the context function to take a third parameter, and giving the expression language the ability to specify such a parameter.
  Attribute caching is achieved by changing our semantics to ``write'' the computed value of an attribute to the context function during evaluation.
  This required the addition of the cache expression, a production of the expression language intended only for intermediary use during evaluation.
  If we had specified big step semantics only, such an expression would not have been necessary, but we were motivated to specify small-step semantics to allow for better quantitative analysis.
  
  \item[] Higher order attributes are presented in \cref{chap-saiga-ho}.
  We implement higher order semantics by providing an expression form that specifies a set of attribute equation expressions to be ``written'' to the context function.
  This generalised approach allows higher order attributes to be specified in a way that does or does not ``tie in'' to the existing tree, a generalisation that is important as different attribute grammar platforms differ widely in this regard.
  The open and ``tree-structure-agnostic'' nature of the context function allowed higher order attribution to be added to our semantics with relative ease; as the tree structure is not being tracked in any way, modifying the structure can happen without any hassle.
  
  \item \quotecontrib{Our calculus represents a framework for reasoning about and comparing the behaviour of attribute grammar programs, aided by a set of \contribemph{metatheoretic properties}, for which we provide comprehensive proofs.}
  \item[] \cref{chap-meta} outlines and proves a number of metatheoretic properties about Saiga, starting with simple properties such as determinism and progress, as well as proven equivalence between the small-step, multistep, and big step semantics.
  The most complex property to prove was cache irrelevance -- the property that writing computed attribute values to the cache will not change the semantic outputs of a context function.
  
  
  \item \quotecontrib{The calculus itself, as well as proofs for the majority of these metatheoretic properties, are \contribemph{mechanised} in Lean.}
  \item[] We present our complete Lean code via external repositories, presenting only small examples in \cref{chap-mechanisation}.
  The core and extended versions of Saiga were mechanised with ease, including their metatheoretic proofs.
  Proof of cache irrelevance for the extended calculus was mechanised, although not without considerable effort.
  
  The higher order calculus was also mechanised, with proofs of most of its metatheoretic properties similarly mechanised.
  The nature of a changing context function, especially when it comes to nodes ``existing'' or ``not existing'' in a black-box mathematical function, presented considerable difficulties during mechanisation.
  As a result, our mechanisation does not include a complete proof of cache irrelevance for the higher order semantics.
  We believe this proof is possible to mechanise, given enough effort (see \cref{sect-conclusion-futurework}).
  
  \item \quotecontrib{We \contribemph{demonstrate the utility} of these techniques through analysis of a real-world scale problem: comparing two different approaches to name and type analysis for Featherweight Java, translated directly from implementations in two different real-world attribute grammar platforms.}
  \item[] A major contribution of this thesis, \cref{chap-example} considers the Featherweight Java language, specifies two different approaches to name and type analysis written for two different attribute grammar platforms, and proves them to be equivalent.
  Thanks to the maintainers of Kiama and JastAdd, who were kind enough to provide implementations of Featherweight Java in their own platforms, we were able to translate the two different approaches into Saiga specifications, and prove that one particular attribute would always evaluate to the same result, for any Featherweight Java program.
  
  \item \quotecontrib{This comparison is primarily a proof that two particular attributes always evaluate to the same value, but we also demonstrate Saiga's facility for \contribemph{quantitative analysis} by comparing the number of evaluation steps taken for a particular computation.}
  \item[] We continue our analysis in \cref{chap-example} by proving that name binding on Featherweight Java identifier expressions complete in a number of evaluation steps that conform to a specific formula.
  We show that JastAdd's approach is more efficient in the best-case scenario (when a name's binding is in its smallest containing scope), and that the two approaches are very close to identical in efficiency for the worst-case scenario (when the name is not found in scope).
  
  This is a major contribution, as the Kiama and JastAdd implementations use two different but accepted name analysis strategies -- the ``environment'' and ``lookup'' strategies.
  Before Saiga, there was no framework for formally analysing such strategies, and the result that their worst-case time complexities are close to identical is an interesting one.
  Much more analysis could be performed to compare the effects of attribute caching on repeated name lookup evaluations -- the example presented in \cref{chap-example} is merely a demonstration of the kinds of analysis that can be performed using the Saiga calculus.
\end{enumerate}

  



\section{Future Work}\label{sect-conclusion-futurework}
  Based on the work presented in this thesis, there are a number of directions for potential future work.
\begin{itemize}
  \item \tb{Circular attributes could be implemented using iterative fixed-point evaluation.}
  This would probably involve the context function returning not only an expression to represent the attribute equation, but also some indication of a starting value for fixed-point computation, perhaps as an Option so that not all attributes allow fixed-point circular evaluation.
  The fact that normal evaluation wraps all non-value attribute computations in cache expressions provides an existing ``log'' of the nested stack of attributes being evaluated, which would be a useful starting point for fixed-point evaluation.
  However in the current approach to our semantics, each expression is evaluated without regard for what expression contains it.
  To implement fixed-point circular evaluation, we would need to include some extra contextual information to our step relation, to allow evaluation to know when a cycle has been reached.
  
  \item \tb{Nodes and attributes could be extended with type information, so that not all attributes are applicable to all nodes.}
  In the version of Saiga presented in this thesis, we abstract all information away from a node, including its structure and its type.
  This abstraction has proved useful in providing the flexibility to specify attribute grammar programs without being bound to a particular type system, but we have found that we have been implementing a simple type system inside Saiga in every example we have presented, which may be seen as an indicator that the type system should be part of the calculus.
  It would be possible to tag each instance of \N and each instance of \A with a simple enumerated type, and redefine a context function in the following way, such that \sig is still a mathematical (total) function, but not all attributes need to be defined on all nodes.
  \[
    \sig \in \N[q] -> (a: \A[q]) -> \rho(a) -> \E
  \]
  Further, this would allow the context function to return different expressions for nodes of different types, without reflecting on intrinsic attributes such as \a{nodeType}.
  We opted not to take this approach in the work presented in this thesis, instead choosing to present a simpler calculus.
  Nevertheless, node types are an important part of almost all real-world attribute grammar program specifications, so our calculus might benefit from modelling them as an explicit part of the calculus, instead of modelling them using the calculus.
  
  \item \tb{Our mechanisation of higher order attributes could be improved.}
  Our experiences with mechanising Saiga, both in the original Coq version and now in Lean, has been that grammars and relations are easy to model as inductive datatypes.
  The context function, on the other hand, is designed specifically to be a black-box function, which does not play so well with our mechanisation tools.
  In our approaches to mechanisation we have implemented a concrete base context function, and created specific functions to implement the $\oplus$ and $\otimes$ operations.
  This introduces a kind of process ordering to our context functions that does not naturally occur in our ``on paper'' semantics, and makes proofs about changing context functions more difficult to frame.
  More work could be done in expressing the context function and the way it changes in our mechanisation, in such a way that allows for more convenient reasoning.
\end{itemize}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  